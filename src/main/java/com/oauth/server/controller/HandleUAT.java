package com.oauth.server.controller;

import com.oauth.server.bootstrap.OAuth2Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@RestController
@RequestMapping(value = "/uat", produces = "application/json")
public class HandleUAT {

    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private OAuth2Configuration oauth2Configuration;

    @GetMapping(value = "/validate-token")
    private ResponseEntity<Boolean> validateUAT(HttpServletRequest request) {
        OAuth2AccessToken oAuth2AccessToken = getOAuth2AccessToken(request);
        boolean isNotExpired = validateAccessToken(oAuth2AccessToken);
        return new ResponseEntity<>(isNotExpired, HttpStatus.OK);
    }

    @GetMapping(value = "/validate-token-by-username")
    private ResponseEntity<Boolean> validateUATByName(String username) {

        String clientId = oauth2Configuration.getPassword().getId();
        Collection<OAuth2AccessToken> oAuth2AccessTokens = tokenStore.findTokensByClientIdAndUserName(clientId, username);
        for (OAuth2AccessToken oAuth2AccessToken : oAuth2AccessTokens) {
            validateAccessToken(oAuth2AccessToken);
        }
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    //@PreAuthorize("hasIpAddress('52.58.148.215')")
    @GetMapping(value = "/revoke-token")
    @ResponseStatus(HttpStatus.OK)
    public void destroyUAT(HttpServletRequest request) {
        OAuth2AccessToken oAuth2AccessToken = getOAuth2AccessToken(request);
        tokenStore.removeAccessToken(oAuth2AccessToken);
    }

    private OAuth2AccessToken getOAuth2AccessToken(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader == null)
            throw new OAuth2AccessDeniedException("");
        String tokenValue = authHeader.replace("Bearer", "").trim();
        return tokenStore.readAccessToken(tokenValue);
    }

    private boolean validateAccessToken(OAuth2AccessToken oAuth2AccessToken){
        if (oAuth2AccessToken.isExpired())
            throw new IllegalArgumentException("");
        return true;
    }
}
