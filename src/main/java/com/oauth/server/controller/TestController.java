package com.oauth.server.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/test", produces = "application/json")
public class TestController {

    @GetMapping(value = "/testme")
    public ResponseEntity<String> testMethod() {
        return new ResponseEntity<>("Appel papa hein", HttpStatus.OK);
    }
}
