package com.oauth.server.security;

import com.kazi.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userServiceImpl;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final com.kazi.api.details.UserDetails details = userServiceImpl.findByUsername(username);
        if (details == null)
            throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
        return new UserRepositoryUserDetails(details);
    }

    private final static class UserRepositoryUserDetails extends com.kazi.api.details.UserDetails implements UserDetails {

        private static final long serialVersionUID = 1L;

        private UserRepositoryUserDetails(com.kazi.api.details.UserDetails details) {
            super(details);
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return Collections.singletonList(getRole());
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return !super.isBlocked();
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }
    }
}
