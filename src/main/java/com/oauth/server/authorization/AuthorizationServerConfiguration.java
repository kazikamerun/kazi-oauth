package com.oauth.server.authorization;

import com.oauth.server.bootstrap.OAuth2Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    private static final String SCOPE_WRITE = "write";

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private OAuth2Configuration oauth2Configuration;

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(oauth2Configuration.getPassword().getId())
                .secret(passwordEncoder.encode(oauth2Configuration.getPassword().getSecret()))
                .authorizedGrantTypes("password", "refresh_token")
                .scopes(SCOPE_WRITE)
                .accessTokenValiditySeconds(6000)
                .refreshTokenValiditySeconds(24000)
                .autoApprove(true)
                .and()
                .withClient(oauth2Configuration.getCredentials().getId())
                .authorizedGrantTypes("client_credentials", "refresh_token")
                .scopes(SCOPE_WRITE)
                .secret(passwordEncoder.encode(oauth2Configuration.getCredentials().getSecret()))
                .accessTokenValiditySeconds(6000)
                .refreshTokenValiditySeconds(24000)
                .autoApprove(true);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore())
                .authenticationManager(authenticationManager);
    }

    @Bean
    public TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }
}