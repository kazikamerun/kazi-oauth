package com.oauth.server.beans;

import com.kazi.api.service.RegistrationService;
import com.kazi.api.service.UserService;
import com.kazi.service.impl.RegistrationServiceImpl;
import com.kazi.service.impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OAuthBeans {

    @Bean
    public UserService userServiceImpl() {
        return new UserServiceImpl();
    }

    @Bean
    public RegistrationService registrationService() {
        return new RegistrationServiceImpl();
    }
}
