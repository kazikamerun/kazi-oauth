#! /bin/bash

docker build -t kazi-oauth-k8s .

#minikube image build -t kazi-oauth-k8s -f ./Dockerfile .

minikube image load kazi-oauth-k8s

kubectl apply -f deployment/service.yaml

kubectl apply -f deployment/deployment.yaml