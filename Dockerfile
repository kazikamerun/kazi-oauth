FROM adoptopenjdk/openjdk11
WORKDIR /opt
ENV PORT 8082
EXPOSE 8082
COPY target/*-exec.jar /opt/kazi-oauth.jar
ENTRYPOINT ["java", "-jar", "kazi-oauth.jar"]