#! /bin/bash

echo "delete kazi-oauth-service"
kubectl delete service kazi-oauth-service

echo "\ndelete kazi-oauth-deployment"
kubectl delete -n default deployment kazi-oauth-deployment

echo "\nremove kazi-oauth-k8s minkube image"
minikube image rm kazi-oauth-k8s

#echo "\nremove kazi-oauth-k8s docker image"
#docker rmi kazi-oauth-k8s