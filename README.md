curl -v GET -H "Accept: application/json" http://localhost:8080/test/testme


## Get Access token
### insert default user into the db 
`INSERT INTO user(username, password, enabled, blocked, role) VALUES ('emoleumassi', '$2a$04$28X.Ca4S3bt.n01PFnxNq.RqMD/c.OImYfM2HWXm48FbFyydpRWey', false, false, 'ADMIN');`

###curl
`curl -i -v POST -H 'Content-Type: application/x-www-form-urlencoded' -k http://localhost:8082/oauth/token -H 'Authorization: Basic Y2xpZW50OnNlY3JldA==' -d 'grant_type=password&client_id=client&username=emoleumassi&password=today&scope=write'`
